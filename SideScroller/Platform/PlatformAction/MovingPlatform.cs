﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class MovingPlatform : MonoBehaviour {

    public bool goingOnPositiveDirection = true;

    public float movespeed;

    public float maxPosition;
    public float minPosition;

    protected float minTargetPosition;
    protected float maxTargetPosition;

    float counter = 0f;
    
    public float waitTime = 0f;

    bool waiting = false;
    
    void Start () {
        Initialize();
    }
    
    void Update () {
        if (waiting) {
            Wait();
        } else {
            TryAndMove();
        }
    }

    void TryAndMove() {
        if (OutOfBounds()) {
            ChangeDirection();
            if (waitTime >= 0f) {
                waiting = true;
                counter = waitTime;
            }
        } else {
            Move();
        }
    }

    void ChangeDirection() {
        goingOnPositiveDirection = !goingOnPositiveDirection;
    }

    void Wait() {
        if (counter <= 0f) {
            waiting = false;
        } else {
            counter -= Time.deltaTime;
        }
    }

    abstract protected bool OutOfBounds();

    abstract protected void Move();

    abstract protected void Initialize();

}
