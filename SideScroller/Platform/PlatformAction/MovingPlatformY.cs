﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformY : MovingPlatform {

    protected override void Initialize() {
        minTargetPosition = transform.position.y + minPosition;
        maxTargetPosition = transform.position.y + maxPosition;
    }

    protected override void Move() {
        float directionMultiplier = goingOnPositiveDirection ? 1 : -1;
        transform.Translate(new Vector2(0f, movespeed * directionMultiplier * Time.deltaTime));
    }

    protected override bool OutOfBounds() {
        if (goingOnPositiveDirection)
            return transform.position.y >= maxTargetPosition;
        else
            return transform.position.y <= minTargetPosition;
    }

}
