﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformX : MovingPlatform {

    protected override void Initialize() {
        minTargetPosition = transform.position.x + minPosition;
        maxTargetPosition = transform.position.x + maxPosition;
    }

    protected override void Move() {
        float directionMultiplier = goingOnPositiveDirection ? 1 : -1;
        transform.Translate(new Vector2(movespeed * directionMultiplier * Time.deltaTime, 0f));
    }

    protected override bool OutOfBounds() {
        if (goingOnPositiveDirection)
            return transform.position.x >= maxTargetPosition;
        else
            return transform.position.x <= minTargetPosition;
    }

}
