﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformBlock : MonoBehaviour {

    bool NextBlockSpawned = false;

    public List<GameObject> nextBlocks = new List<GameObject>();

    GameObject GetNextBlock() {
        return nextBlocks[Random.Range(0, nextBlocks.Count)];
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (!NextBlockSpawned && collider.gameObject.tag == "Player") {
            NextBlockSpawned = true;
            PlatformSpawner.get.SpawnBlock(GetNextBlock());
        }
    }

}
