﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpawner : MonoBehaviour {

    static public PlatformSpawner get;

    public List<GameObject> startingBlocks = new List<GameObject>();

    public float distanceBetweenBlocks = 15f;

    public Vector2 spawnPosition;

    void Awake() {
        if (get == null) {
            get = this;
        } else if (get != this) {
            Destroy(get);
        }
    }
    
    void Start() {
        foreach (GameObject block in startingBlocks) SpawnBlock(block);
    }

    public void SpawnBlock(GameObject block) {
        // instantiate the block object
        Instantiate(block, spawnPosition, block.transform.rotation);
        // update next spawn position
        spawnPosition.x += distanceBetweenBlocks;
    }

}
