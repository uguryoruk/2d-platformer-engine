﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LootSpawnData {

    public GameObject loot;
    public Vector3 position;

}
