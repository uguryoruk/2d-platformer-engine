﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyAsLoot : Loot {

    public Currency currency;
    public float value;

    protected override void Collect() {
        PlayerCurrencyManager.get.AddCurrency(currency, value);
        Destroy(gameObject);
    }

}
