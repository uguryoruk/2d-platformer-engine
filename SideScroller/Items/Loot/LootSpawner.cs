﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class LootSpawner : MonoBehaviour {

    public static LootSpawner get;

    public List<GameObject> lootBlocks = new List<GameObject>();

    void Awake() {
        if (get == null) {
            get = this;
        } else if (get != this) {
            Destroy(get);
        }
    }

    void SpawnBlock() {

    }

    void SpawnLoot() {

    }

}
