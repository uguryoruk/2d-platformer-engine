﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootBlock : ScriptableObject {

    [SerializeField]
    public List<LootSpawnData> spawnData = new List<LootSpawnData>();

}
