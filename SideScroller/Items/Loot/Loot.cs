﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Loot : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.gameObject.tag == "Player") {
            Collect();
        }
    }

    abstract protected void Collect();

}
