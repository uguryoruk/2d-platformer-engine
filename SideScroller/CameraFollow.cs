﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public float MinY = -0.7f;
    public float MaxY = 0.7f;

    public float dampTime = 0.15f;
    private Vector3 velocity = Vector3.zero;
    public Transform target;

    float Y;


	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (target.position.y > MaxY) {
            Y = MaxY;
        } else if (target.position.y < MinY) {
            Y = MinY;
        } else {
            Y = target.position.y;
        }
        transform.position = Vector3.SmoothDamp(
            transform.position, new Vector3(target.position.x, Y, transform.position.z), ref velocity, dampTime);
    }
}
