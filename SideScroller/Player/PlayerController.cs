﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public enum STATE {
        WALK,
        JUMP,
        RUN,
        IDLE,
        USE_SKILL
    }
    public STATE state = STATE.IDLE;
    public STATE previousState = STATE.IDLE;

    Rigidbody2D rb;
    SpriteRenderer sr;
    Animator an;

    public float horizontalSpeed = 3f;
    public float jumpSpeed = 5f;

    public string Walk;
    public string Idle;
    public string Run;
    public string Jump;
    public string Skill;

    public Skill currentSkill;
    
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        an = GetComponent<Animator>();
    }
	
    void Update () {

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        switch (state) {

            case STATE.IDLE:
                CheckForMoveRequest(v, h);
                CheckForJumpRequest();
                break;

            case STATE.WALK:
                MoveHorizontally(h);
                CheckForJumpRequest();
                CheckForIdleRequest(v, h);
                break;

            case STATE.RUN:
                MoveHorizontally(h);
                CheckForJumpRequest();
                CheckForIdleRequest(v, h);
                break;

            case STATE.JUMP: // collusion detection is used to check if player hits the ground
                MoveHorizontally(h);
                CheckForJumpRequest();
                break;

            case STATE.USE_SKILL:
                UseSkill();
                break;

            default:
                break;

        }
		
    }

    void CheckForIdleRequest(float v, float y) {
        if (v == 0 && y == 0) ChangeState(STATE.IDLE);
    }

    void CheckForMoveRequest(float v, float h) {

        if (h > 0) {
            sr.flipX = false;
            ChangeState(STATE.WALK);

        } else if (h < 0) {
            sr.flipX = true;
            ChangeState(STATE.WALK);

        }

    }

    void MoveHorizontally(float h) {
        float x = horizontalSpeed * h;
        transform.Translate(new Vector2(x * Time.deltaTime, 0f));
    }

    void CheckForJumpRequest() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            ChangeState(STATE.JUMP);
            rb.velocity = new Vector2(0f, jumpSpeed);
        }
    }

    public bool CanUseSkill() {
        return state != STATE.JUMP && state != STATE.USE_SKILL;
    }

    void TryUseSkill(Skill skill) {
        if (currentSkill == null) {
            currentSkill = skill;
            Skill = skill.animationName;
            skill.Enable();
            ChangeState(STATE.USE_SKILL);
        }
    }

    void UseSkill() {
        if (currentSkill.enabled) {
            currentSkill.Update();
        } else { 
            ChangeState(previousState);
        }
    }

    void ChangeState(STATE newState) {

        previousState = state;
        state = newState;

        switch (previousState) {

            case STATE.WALK:
                an.SetBool(Walk, false);
                break;

            case STATE.RUN:
                an.SetBool(Run, false);
                break;

            case STATE.IDLE:
                an.SetBool(Idle, false);
                break;

            case STATE.JUMP:
                an.SetBool(Jump, false);
                break;

            case STATE.USE_SKILL:
                an.SetBool(Skill, false);
                currentSkill = null;
                break;

        }

        switch (state) {

            case STATE.WALK:
                an.SetBool(Walk, true);
                break;

            case STATE.RUN:
                an.SetBool(Run, true);
                break;

            case STATE.IDLE:
                an.SetBool(Idle, true);
                break;

            case STATE.JUMP:
                an.SetBool(Jump, true);
                break;

            case STATE.USE_SKILL:
                an.SetBool(Skill, true);
                break;

        }

    }

    void OnCollisionEnter2D(Collision2D collider) {
        if (state == STATE.JUMP) {
            ChangeState(STATE.IDLE);
        }
    }

}
