﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "2dPlatformer/Skill")]
public class Skill : ScriptableObject {

    public enum SKILL_TYPE {
        MELEE_COMBAT,
        PROJECTILE,
        SPELL,
        POWER_UP,
    }
    public SKILL_TYPE skillType;

    public KeyCode key;

    public string skillName;

    public string animationName;
    public float duration;

    public bool enabled = false;

    float counter;

    public void Enable() {
        counter = duration;
        enabled = true;
    }

    public void Update() {
        if (counter <= 0) {
            Disable();
        } else {
            counter -= Time.deltaTime;
        }
    }

    public void Disable() {
        counter = 0f;
        enabled = false;
    }

}
