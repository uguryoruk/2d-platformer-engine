﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkills : MonoBehaviour {

    public List<Skill> skills = new List<Skill>();
    public List<Skill> skillOnCooldown = new List<Skill>();

    public PlayerController controller;

    void Start () {
        controller = GetComponent<PlayerController>();
    }
    
    void Update () {
        CheckForSkillUseRequest();
    }

    void CheckForSkillUseRequest() {
        foreach (Skill skill in skills) {
            if (Input.GetKeyDown(skill.key) && controller.CanUseSkill()) {
                controller.SendMessage("TryUseSkill", skill);
            }
        }
    }

    void OnSkillUseRequested() {

    }

}
