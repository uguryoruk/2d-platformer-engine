﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "2dPlatformer/Currency/Currency")]
public class Currency : ScriptableObject {

    public string currencyName;
    public Sprite icon;

}
