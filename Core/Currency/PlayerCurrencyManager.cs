﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCurrencyManager : MonoBehaviour {

    static public PlayerCurrencyManager get;

    public List<PlayerCurrency> currencies = new List<PlayerCurrency>();

    void Awake() {
        if (get == null) {
            get = this;
        } else if (get != this) {
            Destroy(get);
        }
    }

    void Start() {
        // quick fix, reset all currency
        foreach (PlayerCurrency playerCurrency in currencies) {
            playerCurrency.value = 0;
        }
    }

    public void AddCurrency(Currency currency, float amount) {
        foreach (PlayerCurrency playerCurrency in currencies) {
            if (playerCurrency.currency == currency) {
                playerCurrency.Add(amount);
            }
        }
    }

}
