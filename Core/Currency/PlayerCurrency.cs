﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "2dPlatformer/Currency/PlayerCurrency")]
public class PlayerCurrency : ScriptableObject {

    public Currency currency;
    public float value;

    public void Add(float amount) {
        value += amount;
    }

}
