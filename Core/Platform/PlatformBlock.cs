﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Madrize.Core {

    public class PlatformBlock : MonoBehaviour {

        public float height;
        public bool canTriggerNewSpawn = true;

        void OnTriggerExit2D(Collider2D collider) {
            if (collider.tag == "Player" && canTriggerNewSpawn) {
                // send a "spawn" call
                canTriggerNewSpawn = false;
            }
        }
    }

}