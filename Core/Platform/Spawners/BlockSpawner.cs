﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Madrize.Core {

    public class BlockSpawner : MonoBehaviour {

        static public BlockSpawner get;

        public Vector2 spawnPosition;

        public List<GameObject> blocks = new List<GameObject>();

        void Awake() {
            if (get == null) {
                get = this;
            } else if (get != this) {
                Destroy(get);
            }
        }

        private GameObject GetPlatformContainerToSpawn() {
            // currently just returning the first element
            return blocks[0];
        }

        public void SpawnNextContainer(float height) {
            GameObject next = GetPlatformContainerToSpawn();
            Instantiate(next, spawnPosition, next.transform.rotation);
            spawnPosition.y = spawnPosition.y + height;
        }

    }

}