﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Madrize.Core {

    public class SceneController : MonoBehaviour {

        public void ChangeScene(string name) {
            UnityEngine.SceneManagement.SceneManager.LoadScene(name);
        }

        public void QuitGame() {
            Application.Quit();
        }

    }

}
