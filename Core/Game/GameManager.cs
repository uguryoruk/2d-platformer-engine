﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Madrize.Core {

    public class GameManager : MonoBehaviour {

        static public GameManager get;

        float counter;

        public delegate void TimedInterruptCallback();
        TimedInterruptCallback timedInterruptCallback;

        public enum STATE {
            Start,
            Play,
            Pause,
            TimedInterrupt,
            GameOver
        }
        public STATE currentState = STATE.Start;
        public STATE previousState = STATE.Play;

        void Awake() {
            if (get == null) {
                get = this;
            } else if (get != this) {
                Destroy(get);
            }
        }

        void Update() {

            switch (currentState) {

                case STATE.TimedInterrupt:
                    if (counter <= 0) {
                        currentState = previousState;
                        timedInterruptCallback();
                    } else {
                        counter -= Time.deltaTime;
                    }
                    break;

            }

        }

        public void Pause() {
            ChangeState(STATE.Pause);
        }

        public void GameOver() {
            ChangeState(STATE.GameOver);
        }

        public void Interrupt(float time, TimedInterruptCallback callback) {
            currentState = STATE.TimedInterrupt;
            counter = time;
            timedInterruptCallback = callback;
        }

        public bool CanInteractWithEnvironment() {
            return currentState == STATE.Play;
        }

        public bool CanInteractWithUI() {
            return currentState != STATE.Start;
        }

        public bool PlayerCanMove() {
            return currentState == STATE.Play;
        }

        void ChangeState(STATE state) {
            previousState = currentState;
            currentState = state;
        }

        public bool Playing() {
            return currentState == STATE.Play;
        }

    }


}