﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Madrize.Core;

namespace Madrize.Jumper {

    public class PlatformBlock : MonoBehaviour {

        public float height;
        public bool canTriggerNewSpawn = true;

        void OnTriggerExit2D(Collider2D collider) {
            if (collider.tag == "Player" && canTriggerNewSpawn) {
                BlockSpawner.get.SpawnNextContainer(height);
                canTriggerNewSpawn = false;
            }
        }

    }

}
