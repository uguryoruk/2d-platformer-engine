﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour {
    
    BoxCollider2D boxCollider;

    // Use this for initialization
    void Start () {
        boxCollider = GetComponent<BoxCollider2D>();
    }
    
    // Update is called once per frame
    void Update () {
        
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag == "Player" && collider.GetComponent<Rigidbody2D>().velocity.y < 0) {
            boxCollider.isTrigger = false;
            collider.gameObject.SendMessage("UpdateState", Player.STATE.Stand);
        }
    }

    void OnCollisionEnter2D(Collision2D collider) {
        if (collider.gameObject.tag == "Player") {
            collider.gameObject.SendMessage("UpdateState", Player.STATE.Stand);
        }
    }

    void OnCollisionExit2D(Collision2D collider) {
        if (collider.gameObject.tag == "Player") {
            boxCollider.isTrigger = true;
        }
    }

    bool PlayerIsOnThePlatform(Transform player) {
        return false;
    }

}
