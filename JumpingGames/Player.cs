﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Madrize.Core;

public class Player : MonoBehaviour {

    public enum STATE {
        Stand,
        Jump
    }
    public STATE currentState = STATE.Stand;

    public float HorizontalSpeed;
    public float VerticalPushPower = 12f;

    float ScreenHorizontalCenter = Screen.width / 2;

    Rigidbody2D rb;
    SpriteRenderer sr;
    Animator an;

    public string jumpAnimation;
    public string standAnimation;

    void Start() {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        an = GetComponent<Animator>();
    }
    
    void Update () {

        switch(currentState) {
            
            case STATE.Stand:
                if (GameManager.get.Playing())
                    CheckForJumpRequest();
                break;

            case STATE.Jump:
                break;

        }

    }

    void CheckForTouchInput() {
        if (Input.touchCount > 0) {
            Touch touch = Input.GetTouch(0);
            SetHorizontalDirection(touch.position.x < Screen.width / 2);
        }
    }

    void CheckForJumpRequest() {
        if (Input.GetMouseButtonDown(0)) {
            // move horizontally
            SetHorizontalDirection(Input.mousePosition.x < Screen.width / 2);
            // move vertically
            TryPushUp(VerticalPushPower);
            // change state
            ChangeState(STATE.Jump);
        }
    }

    void ApplyHorizontalMovement(float speed) {
        rb.velocity = new Vector2(speed, rb.velocity.y);
    }

    void SetHorizontalDirection(bool GoingLeft) {
        sr.flipX = GoingLeft;
        float speed =  HorizontalSpeed * CalculateHorizontalSpeedMultiplier();
        speed = GoingLeft ? -1* speed : 1* speed;
        ApplyHorizontalMovement(speed);
    }

    float CalculateHorizontalSpeedMultiplier() {
        return Mathf.Abs(Input.mousePosition.x - ScreenHorizontalCenter) / ScreenHorizontalCenter;
    }

    public void TryPushUp(float power) {
        rb.velocity = new Vector2(rb.velocity.x, power);
    }

    public void UpdateState(STATE state) {
        ChangeState(state);
    }

    void ChangeState(STATE next) {

        STATE previousState = currentState;
        currentState = next;

        switch (next) {

            case STATE.Stand:
                an.SetBool(standAnimation, true);
                break;

            case STATE.Jump:
                an.SetBool(jumpAnimation, true);
                break;

        }

        switch (previousState) {

            case STATE.Stand:
                an.SetBool(standAnimation, false);
                break;

            case STATE.Jump:
                an.SetBool(jumpAnimation, false);
                break;

        }

    }

}
